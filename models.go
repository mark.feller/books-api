package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

type BookStatus int

const (
	CheckedIn BookStatus = iota
	CheckedOut
)

func (s BookStatus) MarshalJSON() ([]byte, error) {
	if s == CheckedIn {
		return json.Marshal("CheckedIn")
	}
	return json.Marshal("CheckedOut")
}

func (s *BookStatus) UnmarshalJSON(data []byte) (err error) {
	str := strings.Trim(string(data), "\"")
	switch str {
	case "CheckedIn":
		*s = CheckedIn
	case "CheckedOut":
		*s = CheckedOut
	default:
		err = fmt.Errorf("could not unmarshal BookStatus: %s", str)
	}
	return
}

type Book struct {
	gorm.Model
	Title       *string    `gorm:"not null"`
	Author      *string    `gorm:"not null"`
	Publisher   *string    `gorm:"not null"`
	PublishDate *time.Time `json:"publish_date" gorm:"publish_date"`
	Rating      int
	Status      *BookStatus
}

func (b *Book) Validate() error {
	if b.Rating < 1 || b.Rating > 3 {
		return errors.New("invalid rating")
	}
	return nil
}
