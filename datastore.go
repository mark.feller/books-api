package main

import "context"

type Datastore interface {
	Create(context.Context, *Book) (*Book, error)
	Read(context.Context) ([]Book, error)
	Update(context.Context, *Book) (*Book, error)
	Delete(context.Context, *Book) error
}
