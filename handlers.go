package main

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"strings"
)

func (s *BookServer) BaseHandler(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPut:
		s.CreateHandler(ctx, w, r)
	case http.MethodGet:
		s.ReadHandler(ctx, w, r)
	case http.MethodPost:
		s.UpdateHandler(ctx, w, r)
	case http.MethodDelete:
		s.DeleteHandler(ctx, w, r)
	default:
		respondWithError(ctx, w, errors.New("invalid method"), 404)
	}
}

func (s *BookServer) CreateHandler(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	var book Book
	if err := json.NewDecoder(r.Body).Decode(&book); err != nil {
		respondWithError(ctx, w, err, 400)
		return
	}

	if err := book.Validate(); err != nil {
		respondWithError(ctx, w, err, 400)
		return
	}

	res, err := s.store.Create(ctx, &book)
	if err != nil {
		respondWithError(ctx, w, err, 500)
		return
	}

	b, err := json.Marshal(res)
	if err != nil {
		respondWithError(ctx, w, err, 500)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(b)
	logger(ctx).Println("created book")
}

func (s *BookServer) ReadHandler(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	books, err := s.store.Read(ctx)
	if err != nil {
		respondWithError(ctx, w, err, 500)
		return
	}

	b, err := json.Marshal(books)
	if err != nil {
		respondWithError(ctx, w, err, 500)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(b)
	logger(ctx).Println("read books")
}

func (s *BookServer) UpdateHandler(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	var book Book
	if err := json.NewDecoder(r.Body).Decode(&book); err != nil {
		respondWithError(ctx, w, err, 500)
		return
	}

	if err := book.Validate(); err != nil {
		respondWithError(ctx, w, err, 400)
		return
	}

	res, err := s.store.Update(ctx, &book)
	if err != nil {
		respondWithError(ctx, w, err, 500)
		return
	}

	b, err := json.Marshal(res)
	if err != nil {
		respondWithError(ctx, w, err, 500)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(b)
	logger(ctx).Println("updated book")
}

func (s *BookServer) DeleteHandler(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	var book Book
	if err := json.NewDecoder(r.Body).Decode(&book); err != nil {
		respondWithError(ctx, w, err, 500)
		return
	}

	if err := s.store.Delete(context.TODO(), &book); err != nil {
		respondWithError(ctx, w, err, 500)
		return
	}
}

func respondWithError(ctx context.Context, w http.ResponseWriter, err error, code int) {
	w.Header().Set("Content-Type", "application/json")
	b, e := json.Marshal(map[string]string{"error": err.Error()})
	if e != nil {
		http.Error(w, e.Error(), 500)
	}
	logger(ctx).Printf("responding with error %d: %s", code, err.Error())
	http.Error(w, strings.Trim(string(b), "\n"), code)
}
