CREATE SEQUENCE public.books_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.books_id_seq
OWNER TO postgres;
-- Table: public.books

-- DROP TABLE public.books;

CREATE TABLE public.books
(
    id integer NOT NULL DEFAULT nextval('books_id_seq'::regclass),
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    title text COLLATE pg_catalog."default" NOT NULL,
    author text COLLATE pg_catalog."default" NOT NULL,
    publisher text COLLATE pg_catalog."default" NOT NULL,
    publish_date timestamp with time zone,
    rating integer,
    status integer,
    CONSTRAINT books_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.books
    OWNER to postgres;

-- Index: idx_books_deleted_at

-- DROP INDEX public.idx_books_deleted_at;

CREATE INDEX idx_books_deleted_at
    ON public.books USING btree
    (deleted_at)
    TABLESPACE pg_default;
