# Buidling

Nothing unusual here. Standard `go build`

I've used dep for dependency management and the few depenecies are in the vendor
directory.

The docker image uses a multistage build using the alpine container to create a
very small image.

# Deploying

Deploying into kubernetes requires a few step. First initialize the configuration.

`kubectl create -f config/books.yaml`

Then the db and api deployments can be initialized.

```bash
kubectl create -f deployments/db.yaml
kubectl create -f deployments/books.yaml
```

Using minikube for testing the api can be accessed at

`minikube service books-api --url`

# API

The api is very simple and crude by design. I started off trying to make a more
full featured api, but quickly decided to keep it simple. I ran into feature
bloat issues and given the time constraints I felt it wasn't important to add
things like queries.

Notes:
- Read will simply return all records
- Create will return the record you've just created
- Updated will return the record you've just updated
- Delete will return with a 200 and no body when a delete is successful
- all errors are returned as json with an error field

Currently the use of error codes 400 and 500 are a bit jumbled. These could be
polished to make more sense.

The API listens of port 8080.

# Mocking and Testing

I left room in the design for mocking and having multiple backends using the
datastore interface.

The testing currently just uses a postgres database and it expects the database
to be running at the configured location using the environment variables:
- DBHOST
- DBPORT
- DBUSER
- DBPASS
- DBNAME

# Library Usage

The original intention was to use only the standard libraries; however, while I
was working on the database portion of the code I found myself writing large
amounts of boiler plate code around the books struct. I felt this was really
unecessary and distracting from the actual code. As a result I've used gorm to
provide orm support and make the code much less bloated.

I've also used the package "github.com/pkg/errors" as it allows for a nice way
to wrap and unwrap error messages. Very simple package that only depends on
standard libraries itself.

# Database

I've chosen to use postgresql for the example because I think it's a very nice
database and it is what is being used at Redeam.

Currently configuration for the database connection is done through environment
variables. This is fine for a sample program. For a larger application I would
likely use viper as it provides a more comprehensive configuration management
system.

# Logging

I haven't done much with logging apart from experimenting with the contextual
logging. It uses the standard library, but in a production environment it would
be much nice to have structured logging.

# Comments

I have not gone through and commented the code as of yet. I am more than willing
to go through and comment once I get some time.
