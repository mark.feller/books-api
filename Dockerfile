FROM golang:alpine
WORKDIR /go/src/gitlab.com/mark.feller/books-api
COPY . .
RUN go install

# Final Build Image
FROM alpine
EXPOSE 8080
COPY --from=0 /go/bin/books-api /bin

ENTRYPOINT ["/bin/books-api"]
