// handlers_test.go
package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

var (
	testserver *BookServer
	handler    http.HandlerFunc
)

// func init() {
//	server = NewBookServer(&mockDatastore{})
//	rr = httptest.NewRecorder()
//	handler = http.HandlerFunc(server.BaseHandler)
// }

// Mock datastore can be fleshed out to provide testing when no DB is available
type mockDatastore struct{}

func (m *mockDatastore) Create(ctx context.Context, book *Book) (*Book, error) {
	return &Book{}, nil
}

func (m *mockDatastore) Read(ctx context.Context) ([]Book, error) {
	return make([]Book, 0), nil
}

func (m *mockDatastore) Update(ctx context.Context, book *Book) (*Book, error) {
	return book, nil
}

func (m *mockDatastore) Delete(ctx context.Context, book *Book) error {
	return nil
}

func init() {
	// os.Setenv("DBNAME", "books")

	config := initConfig()
	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s "+
		"password=%s dbname=%s sslmode=disable",
		config["DBHOST"], config["DBPORT"],
		config["DBUSER"], config["DBPASS"], config["DBNAME"])

	db, err := NewPgDatastore(psqlInfo)
	if err != nil {
		log.Fatal(err)
	}

	testserver = NewBookServer(db, "8080")
	handler = http.HandlerFunc(WithContext(LogHandler(testserver.BaseHandler)))
}

func TestRead(t *testing.T) {
	req, err := http.NewRequest("GET", "/books", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	var res []Book
	if err := json.NewDecoder(rr.Body).Decode(&res); err != nil {
		t.Error("could not unmarshal read results:", err)
	}
	t.Logf("read found %d books", len(res))
}

func TestBadMethod(t *testing.T) {
	req, err := http.NewRequest("UPDATE", "/books", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != 404 {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, 404)
	}

	expected := `{"error":"invalid method"}
`
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %s want %s",
			rr.Body.String(), expected)
	}
}

func TestBadCreate(t *testing.T) {
	body := bytes.NewBuffer([]byte(`{
"title": "test book",
"rating": 5}`))

	req, err := http.NewRequest("PUT", "/books", body)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != 400 {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, 400)
	}

	expected := `{"error":"invalid rating"}
`
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %s want %s",
			rr.Body.String(), expected)
	}
}

func TestBadDBCreate(t *testing.T) {
	body := bytes.NewBuffer([]byte(`{
"title": "test book",
"rating": 2}`))

	req, err := http.NewRequest("PUT", "/books", body)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != 500 {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, 500)
		return
	}
}

func TestBadStatus(t *testing.T) {
	body := bytes.NewBuffer([]byte(`{
"title": "New Book",
"author": "Filbert",
"rating": 3,
"publisher": "Test Publisher Inc",
"publish_date": "2018-03-09T00:00:00.0000000-00:00",
"status": "Bogus"
}`))

	req, err := http.NewRequest("PUT", "/books", body)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != 400 {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, 400)
	}

	expected := `{"error":"could not unmarshal BookStatus: Bogus"}
`
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %s want %s",
			rr.Body.String(), expected)
	}
}

func TestCreate(t *testing.T) {
	body := bytes.NewBuffer([]byte(`{
"title": "New Book",
"author": "Filbert",
"rating": 3,
"publisher": "Test Publisher Inc",
"publish_date": "2018-03-09T00:00:00.0000000-00:00",
"status": "CheckedOut"
}`))

	req, err := http.NewRequest("PUT", "/books", body)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != 200 {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, 200)
	}
}

func TestBadUpdate(t *testing.T) {
	body := bytes.NewBuffer([]byte(`{
"id":6666,
"title": "New Book",
"author": "Filbert",
"rating": 3,
"publisher": "Test Publisher Inc",
"publish_date": "2018-03-09T00:00:00.0000000-00:00",
"status": "CheckedIn"
}`))

	req, err := http.NewRequest("POST", "/books", body)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != 500 {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, 500)
		return
	}

	expected := `{"error":"could not update record: record not found"}
`
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %s want %s",
			rr.Body.String(), expected)
	}
}

func TestUpdate(t *testing.T) {
	req, err := http.NewRequest("GET", "/books", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	var res []Book
	if err := json.NewDecoder(rr.Body).Decode(&res); err != nil {
		t.Error("could not unmarshal read results:", err)
	}

	if len(res) == 0 {
		t.Fatal("no books to update")
	}

	title := "updated title"
	res[0].Title = &title
	body := bytes.NewBuffer(nil)
	if err := json.NewEncoder(body).Encode(res[0]); err != nil {
		t.Fatal(err)
	}

	req, err = http.NewRequest("POST", "/books", body)
	if err != nil {
		t.Fatal(err)
	}
	rr = httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != 200 {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, 200)
		return
	}
}
func TestBadDelete(t *testing.T) {
	body := bytes.NewBuffer([]byte(`{
"id":6666
}`))

	req, err := http.NewRequest("DELETE", "/books", body)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != 500 {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, 500)
		return
	}

	expected := `{"error":"could not delete record: record not found"}
`
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %s want %s",
			rr.Body.String(), expected)
	}
}

func TestDelete(t *testing.T) {
	req, err := http.NewRequest("GET", "/books", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	var res []Book
	if err := json.NewDecoder(rr.Body).Decode(&res); err != nil {
		t.Error("could not unmarshal read results:", err)
	}

	if len(res) == 0 {
		t.Fatal("no books to delete")
	}

	body := bytes.NewBuffer(nil)
	if err := json.NewEncoder(body).Encode(res[0]); err != nil {
		t.Fatal(err)
	}

	req, err = http.NewRequest("DELETE", "/books", body)
	if err != nil {
		t.Fatal(err)
	}
	rr = httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != 200 {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, 200)
		return
	}
}
