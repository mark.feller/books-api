package main

import (
	"context"

	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
	"github.com/pkg/errors"
)

type PgDatastore struct {
	conn string
}

func NewPgDatastore(conn string) (*PgDatastore, error) {
	return &PgDatastore{conn: conn}, nil
}

func (ds *PgDatastore) Create(ctx context.Context, book *Book) (*Book, error) {
	db, err := gorm.Open("postgres", ds.conn)
	if err != nil {
		return nil, err
	}
	defer db.Close()
	if err := db.Create(book).Error; err != nil {
		return nil, errors.Wrap(err, "could not create record")
	}
	return book, nil
}

func (ds *PgDatastore) Read(ctx context.Context) (res []Book, err error) {
	db, err := gorm.Open("postgres", ds.conn)
	if err != nil {
		return nil, err
	}
	defer db.Close()
	if e := db.Find(&res).Error; e != nil {
		err = errors.Wrap(err, "could not read datastore")
	}
	return
}

func (ds *PgDatastore) Update(ctx context.Context, book *Book) (*Book, error) {
	db, err := gorm.Open("postgres", ds.conn)
	if err != nil {
		return nil, err
	}
	defer db.Close()
	if db.First(book).RecordNotFound() {
		return nil, errors.New("could not update record: record not found")
	}
	if err := db.Save(book).Error; err != nil {
		return nil, errors.Wrap(err, "could not update record")
	}
	return book, nil
}

func (ds *PgDatastore) Delete(ctx context.Context, book *Book) error {
	db, err := gorm.Open("postgres", ds.conn)
	if err != nil {
		return err
	}
	defer db.Close()
	if db.First(book).RecordNotFound() {
		return errors.New("could not delete record: record not found")
	}
	if err := db.Delete(book).Error; err != nil {
		return errors.Wrap(err, "could not delete record")
	}
	return nil
}
