package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
)

func LogHandler(handler func(context.Context, http.ResponseWriter, *http.Request)) func(context.Context, http.ResponseWriter, *http.Request) {
	return func(ctx context.Context, w http.ResponseWriter, r *http.Request) {
		prefix := fmt.Sprintf("[%s] ", r.RemoteAddr)
		l := log.New(os.Stdout, prefix, log.LstdFlags)
		ctx = logContext(ctx, l)

		logger(ctx).Println("recieved request")
		handler(ctx, w, r)
	}
}

func WithContext(handler func(context.Context, http.ResponseWriter, *http.Request)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		handler(context.Background(), w, r)
	}
}

// contextual logging setup

var logKey = struct{}{}

func logContext(ctx context.Context, log *log.Logger) context.Context {
	return context.WithValue(ctx, logKey, log)
}

func logger(ctx context.Context) *log.Logger {
	l, ok := ctx.Value(logKey).(*log.Logger)
	if !ok {
		return log.New(os.Stdout, "", log.LstdFlags)
	}
	return l
}
