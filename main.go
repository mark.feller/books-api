package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

var server *BookServer

func init() {
	// port := flag.String("port", "8080", "port to listen on")
	// flag.Parse()
	port := "8080"

	config := initConfig()
	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s "+
		"password=%s dbname=%s sslmode=disable",
		config["DBHOST"], config["DBPORT"],
		config["DBUSER"], config["DBPASS"], config["DBNAME"])

	db, err := NewPgDatastore(psqlInfo)
	if err != nil {
		log.Fatal(err)
	}

	// port is currently not used
	server = NewBookServer(db, port)
}

func main() {
	log.Printf("listening on %s", server.port)
	log.Fatal(server.ListenAndServe())
}

type config map[string]string

func initConfig() config {
	c := make(config)
	c.getConfig("DBHOST", "localhost")
	c.getConfig("DBPORT", "5432")
	c.getConfig("DBUSER", "postgres")
	c.getConfig("DBPASS", "password")
	c.getConfig("DBNAME", "postgres")
	return c
}

func (c config) getConfig(env string, def string) {
	if v := os.Getenv(env); v != "" {
		c[env] = v
	} else {
		c[env] = def
	}
}

type BookServer struct {
	store  Datastore
	server *http.Server
	port   string
}

func NewBookServer(store Datastore, port string) *BookServer {
	server := &BookServer{store: store, port: port}

	mux := &http.ServeMux{}
	mux.HandleFunc("/books", WithContext(LogHandler(server.BaseHandler)))

	server.server = &http.Server{
		Addr:    ":8080",
		Handler: mux,
	}

	return server
}

func (s *BookServer) ListenAndServe() error {
	return s.server.ListenAndServe()
}
